<?php

namespace App\Controller;

use App\Entity\Project;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;


class PortfolioController extends AbstractController
{
 /**
     * @Route("/index", name="index")
     */
    public function index() :Response
    {
        $projects = $this->getDoctrine()->getRepository(Project::class)->findAll();


        return $this->render("base.html.twig",array(
            'projects' => $projects,
        ));

    
    }

    /**
     * @Route("/show/{id}", name="showAction")
     */
    public function show($id) : Response
    {
        $project = $this->getDoctrine()->getRepository(Project::class)->find($id);

        return $this->render('projects/show.html.twig',[
            'project' => $project,
        ]);

    }

    /**
     * @Route("/new", name="new")
     */
    public function new(Request $request): Response
    {

        $project = new Project();

        
        $form = $this->createFormBuilder($project)
            ->add('title',TextType::class)
            ->add('description',TextType::class)
            ->add('save',SubmitType::class,array('label' => 'Create Project'))
            ->getForm();

        $form->handleRequest($request);
        
        if ($form->isSubmitted() && $form->isValid()) { 
            $project = $form->getData();
            $entityManager = $this->getDoctrine()->getManager();

            $entityManager->persist($project);
            $entityManager->flush();

            return $this->redirectToRoute('index');
        }
        
        return $this->render('projects/new.html.twig',[
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/show/{id}", name="show_action")
     */
}
